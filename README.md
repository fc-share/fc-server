# Introduce
 **Open Server Panel** -  is a portable server platform and a software environment designed specifically for web developers, taking into account their recommendations and wishes.
 The software complex has a rich set of server software, a convenient, multifunctional thoughtful interface, has powerful capabilities for administering and configuring components. The platform is widely used to develop, debug and test web projects, as well as to provide Web services on local networks.
 Although initially the software products included in the complex were not designed specifically to work with each other, this bundle became very popular among Windows users, primarily because they received a free software package with reliability at the level of Linux servers.
 Convenience and ease of management certainly will not leave you indifferent, for the time of its existence Open Server has established itself as a first-class and reliable tool necessary for every web master.
 
> This is a version that has been customized by **FC Dev Team** includes
> some rather powerful built-in tools for helping developers easily
> work.

![Screen Shot](https://i.imgur.com/4MvCWd1.png)
![Screen Shot](https://i.imgur.com/qjGHUxS.gif)

# Download
Link [.zip](https://gitlab.com/fc-share/fc-server/-/archive/master/fc-server-master.zip) | [.tar.gz](https://gitlab.com/fc-share/fc-server/-/archive/master/fc-server-master.tar.gz) | [.tar.bz2](https://gitlab.com/fc-share/fc-server/-/archive/master/fc-server-master.tar.bz2) | [.tar](https://gitlab.com/fc-share/fc-server/-/archive/master/fc-server-master.tar) **(1.3GB)**

Git Clone: `git clone git@gitlab.com:fc-share/fc-server.git`

## How to use

Start **Open Server** and run link `http://localhost`

## Composition of the program complex

 -    OSPanel 5.2.8
-   Apache 2.2.34 / 2.4.29
-   Nginx 1.11.7 / 1.12.2
-   MySQL 5.1.73 / 5.5.58 / 5.6.38 / 5.7.20
-   MariaDB 5.5.58 / 10.0.33 / 10.1.30 / 10.2.11
-   MongoDB 2.4.14 / 2.6.12 / 3.0.15 / 3.2.18 / 3.4.10 / 3.6.1
-   PostgreSQL 9.2.24 / 9.3.20 / 9.4.15 / 9.5.10 / 9.6.6 / 9.10.1
-   Redis 2.8.2402 / 3.0.504 / 3.2.100
-   Memcached 1.2.6 / 1.4.5
-   FTP FileZilla 0.9.59
-   PHP 5.2.17 (Zend Optimizer 3.3.3, IonCube 4.0.7, Memcache 2.2.4)
-   PHP 5.3.29 (Xdebug 2.2.7, Memcache 3.0.8, Mongo 1.6.14, Redis 2.2.7, Imagick 3.2.0)
-   PHP 5.4.45 (Xdebug 2.4.1, Memcache 3.0.8, Mongo 1.6.14, Redis 2.2.7, Imagick 3.2.0)
-   PHP 5.5.38 (Xdebug 2.5.5, Memcache 3.0.8, Mongo 1.6.14, MongoDB 1.2.9, Redis 2.2.7, Imagick 3.2.0)
-   PHP 5.6.32 (Xdebug 2.5.5, Memcache 3.0.8, Mongo 1.6.16, MongoDB 1.3.4, Redis 2.2.7, Imagick 3.2.0)
-   PHP 7.0.26 (Xdebug 2.5.5, PDFlib 9.1.1p3, MongoDB 1.3.4, Redis 3.1.5, Phalcon 3.3)
-   PHP 7.1.12 (Xdebug 2.5.5, PDFlib 9.1.1p3, MongoDB 1.3.4, Redis 3.1.5, Phalcon 3.3)
-   PHP 7.2.00 (PDFlib 9.1.1p3, MongoDB 1.3.4, Redis 3.1.5)
-   ImageMagick 6.8.9-9-Q16
-   Bind 9.10.6
-   Git 2.15.1.2
-   Ghostscript 9.22
-   Sendmail 32
-   Wget 1.11.4
-   NNCron Lite 1.17
-   Adminer 4.3.1
-   ConEmu 17.11.09
-   HeidiSQL 9.5
-   RockMongo 1.1.7
-   PHPRedisAdmin 1.9
-   PHPMyAdmin 4.7.3
-   PHPPgAdmin 5.2
-   PHPMemcachedAdmin 1.3

> Components of the assembly are presented in only 64-bit version.

## System requirements

 -    Supported versions of Windows x64: Windows 7 SP1 and all newer versions
-   Partially supported versions of Windows x64: Windows XP SP3 and Windows Vista
-   Minimum hardware requirements: 500 MB of free RAM and 3 GB of free space on the HDD
-   Requires Microsoft Visual C ++ 2005-2008-2010-2012-2013-2015 Redistributable Package

##  Features of the control program

 -   Stealth work in the Windows tray
-   Quick start and stop
-   Autostart server when the program starts
-   Multiple domains management modes
-   Mounting a virtual disk
-   Support for management through the command line
-   Support for configuration profiles
-   Easy viewing of logs of all components
-   Switching HTTP, MySQL and PHP modules
-   Detailed and clear documentation
-   Access to domains in one click
-   Quick access to configuration templates
-   Multilanguage interface
-   Autorun programs on the list

## Features of the complex

 -  Does not require installation (portability)
-   Ability to work with USB storage
-   Simultaneous work with Denwer, Xampp, etc.
-   Work on local / network / external IP address
-   SSL support without any additional. settings
-   Create a domain by creating a regular folder
-   Support for Cyrillic domains
-   Support for aliases (domain pointers)
-   Protection of the server from external access
-   Punycode converter of domain names
-   A package of more than 40 portable programs
-   Task Scheduler (cron)
-   Create a local subdomain without losing the visibility of the primary domain on the Internet

## Included programs

 1. **Default**
	-   FileZilla - FTP client
	-   WinSCP - SCP client
	-   Notepad++ - Code editor
 2. **Web**
	-   Pgweb - Web-based PostgreSQL database browser
    -   Regex Pixie - Crafting and utilizing Regular Expressions
    -   RegEx Tester - Regular Expressions Tester
 3. **Tools**
	 -   IcoFX - Icon editor
	-   PicPick - Multimaster
	-   PngOptimizer - Graphics optimizer
	-   Instant Eyedropper - Colorpicker
	-   WinHTTrack - Website Copier
	-   WinMerge - Compare files and folders
 4. **Utilities**
	-   AutoRuns - Startup management
	-   CurrPorts - Monitoring of open ports
	-   MyEventViewer - System log
	-   FullEventLogView - Event log viewer
	-   ProcessExplorer - Process management
	-   ProcessMonitor - Process monitoring
	-   Advanced IP Scanner - Network scanner
	-   Cmder - Visual for command-line
 5. **Others**
	 -   KeyDB - Password management
	-   MoneyManagerEX - Money management
	-   ToDoList - Tasklists management

## Web-based tools

 1. **FTP/SFTP**
	 -   Easy FTP
	-   Monsta FTP
	-   Oliver
	-   PHFTP
 2. **MySQL/MariaDB**
	 -   phpMyAdmin
	-   Adminer
	-   MySQLDumper
	-   BigDump
	-   phpLiteAdmin
	-   phpMiniAdmin
	-   MyWebSQL
	-   SQL Buddy
	-   SQLiteManager
	-   MonoQL
 3. **PostgreSQL**
	 -   phpPgAdmin
	-   MyWebSQL
 4. **MongoDB**
    -   RockMongo
 5. **Memcached**
	  -   PHPMemcachedAdmin
 6. **Redis**
	 -   phpRedisAdmin