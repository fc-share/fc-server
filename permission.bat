@echo off
@break off
@title FC Server - Set the necessary permissions for change the hosts file
@cls

SET LOCATION_PATH=%WinDir%\System32\drivers\etc

takeown /F %LOCATION_PATH%\hosts

attrib %LOCATION_PATH%\hosts -s -r -h -a

pause